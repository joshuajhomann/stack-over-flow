# Stack Over Flow

Notes:

The app is built in MVC with storyboards.

The only pod I used is SDDWebImage for the image caching.  I do not cancel the pending requests in prepareForReuse, because SDDWebImage does this for you (see line 64 of UIView + WebCache.m).

The model classes and JSON parsing are code generated from quicktype.io.

I used an unwind segue instead of a protocol to update the list when a user is added, since the only think that matters in this case is the terminal state of the user object.

There we'ren't many design notes on the add user screen so I went with what I consider to be the minimal functional behavior:
1) The scrollview scrolls to the selected textField if it is off screen when the keyboard appears
2) The keyboard is dismissed by tapping in the scrollview or by dragging it
3) The add button is only enabled if you actually provide values for all of the fields
