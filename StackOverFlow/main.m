//
//  main.m
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
