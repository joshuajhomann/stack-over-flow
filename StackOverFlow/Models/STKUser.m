//
//  User.m
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import "STKUser.h"

#define λ(decl, expr) (^(decl) { return (expr); })

static id NSNullify(id _Nullable x) {
    return (x == nil || x == NSNull.null) ? NSNull.null : x;
}

NS_ASSUME_NONNULL_BEGIN

@interface STKPage (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface STKUser (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@interface STKBadgeCounts (JSONConversion)
+ (instancetype)fromJSONDictionary:(NSDictionary *)dict;
- (NSDictionary *)JSONDictionary;
@end

@implementation STKUserType
+ (NSDictionary<NSString *, STKUserType *> *)values
{
    static NSDictionary<NSString *, STKUserType *> *values;
    return values = values ? values : @{
                                        @"moderator": [[STKUserType alloc] initWithValue:@"moderator"],
                                        @"registered": [[STKUserType alloc] initWithValue:@"registered"],
                                        };
}

+ (STKUserType *)moderator { return STKUserType.values[@"moderator"]; }
+ (STKUserType *)registered { return STKUserType.values[@"registered"]; }

+ (instancetype _Nullable)withValue:(NSString *)value
{
    return STKUserType.values[value];
}

- (instancetype)initWithValue:(NSString *)value
{
    if (self = [super init]) _value = value;
    return self;
}

- (NSUInteger)hash { return _value.hash; }
@end

static id map(id collection, id (^f)(id value)) {
    id result = nil;
    if ([collection isKindOfClass:NSArray.class]) {
        result = [NSMutableArray arrayWithCapacity:[collection count]];
        for (id x in collection) [result addObject:f(x)];
    } else if ([collection isKindOfClass:NSDictionary.class]) {
        result = [NSMutableDictionary dictionaryWithCapacity:[collection count]];
        for (id key in collection) [result setObject:f([collection objectForKey:key]) forKey:key];
    }
    return result;
}

#pragma mark - JSON serialization

STKPage *_Nullable STKWelcomeFromData(NSData *data, NSError **error)
{
    @try {
        id json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:error];
        return *error ? nil : [STKPage fromJSONDictionary:json];
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

STKPage *_Nullable STKWelcomeFromJSON(NSString *json, NSStringEncoding encoding, NSError **error)
{
    return STKWelcomeFromData([json dataUsingEncoding:encoding], error);
}

NSData *_Nullable STKWelcomeToData(STKPage *welcome, NSError **error)
{
    @try {
        id json = [welcome JSONDictionary];
        NSData *data = [NSJSONSerialization dataWithJSONObject:json options:kNilOptions error:error];
        return *error ? nil : data;
    } @catch (NSException *exception) {
        *error = [NSError errorWithDomain:@"JSONSerialization" code:-1 userInfo:@{ @"exception": exception }];
        return nil;
    }
}

NSString *_Nullable STKWelcomeToJSON(STKPage *welcome, NSStringEncoding encoding, NSError **error)
{
    NSData *data = STKWelcomeToData(welcome, error);
    return data ? [[NSString alloc] initWithData:data encoding:encoding] : nil;
}

@implementation STKPage
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"items": @"users",
                                                    @"has_more": @"hasMore",
                                                    @"quota_max": @"quotaMax",
                                                    @"quota_remaining": @"quotaRemaining",
                                                    };
}

+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error
{
    return STKWelcomeFromData(data, error);
}

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return STKWelcomeFromJSON(json, encoding, error);
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[STKPage alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
        _users = map(_users, λ(id x, [STKUser fromJSONDictionary:x]));
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = STKPage.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:STKPage.properties.allValues] mutableCopy];

    for (id jsonName in STKPage.properties) {
        id propertyName = STKPage.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }

    [dict addEntriesFromDictionary:@{
                                     @"items": map(_users, λ(id x, [x JSONDictionary])),
                                     @"has_more": _hasMore ? @YES : @NO,
                                     }];

    return dict;
}

- (NSData *_Nullable)toData:(NSError *_Nullable *)error
{
    return STKWelcomeToData(self, error);
}

- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error
{
    return STKWelcomeToJSON(self, encoding, error);
}
@end

@implementation STKUser
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"badge_counts": @"badgeCounts",
                                                    @"account_id": @"accountID",
                                                    @"is_employee": @"isEmployee",
                                                    @"last_modified_date": @"lastModifiedDate",
                                                    @"last_access_date": @"lastAccessDate",
                                                    @"reputation_change_year": @"reputationChangeYear",
                                                    @"reputation_change_quarter": @"reputationChangeQuarter",
                                                    @"reputation_change_month": @"reputationChangeMonth",
                                                    @"reputation_change_week": @"reputationChangeWeek",
                                                    @"reputation_change_day": @"reputationChangeDay",
                                                    @"reputation": @"reputation",
                                                    @"creation_date": @"creationDate",
                                                    @"user_type": @"userType",
                                                    @"user_id": @"userID",
                                                    @"accept_rate": @"acceptRate",
                                                    @"location": @"location",
                                                    @"website_url": @"websiteURL",
                                                    @"link": @"link",
                                                    @"profile_image": @"profileImage",
                                                    @"display_name": @"displayName",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[STKUser alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
        _badgeCounts = [STKBadgeCounts fromJSONDictionary:(id)_badgeCounts];
        _userType = [STKUserType withValue:(id)_userType];
    }
    return self;
}

- (void)setValue:(nullable id)value forKey:(NSString *)key
{
    id resolved = STKUser.properties[key];
    if (resolved) [super setValue:value forKey:resolved];
}

- (NSDictionary *)JSONDictionary
{
    id dict = [[self dictionaryWithValuesForKeys:STKUser.properties.allValues] mutableCopy];

    for (id jsonName in STKUser.properties) {
        id propertyName = STKUser.properties[jsonName];
        if (![jsonName isEqualToString:propertyName]) {
            dict[jsonName] = dict[propertyName];
            [dict removeObjectForKey:propertyName];
        }
    }

    [dict addEntriesFromDictionary:@{
                                     @"badge_counts": [_badgeCounts JSONDictionary],
                                     @"is_employee": _isEmployee ? @YES : @NO,
                                     @"user_type": [_userType value],
                                     }];

    return dict;
}
@end

@implementation STKBadgeCounts
+ (NSDictionary<NSString *, NSString *> *)properties
{
    static NSDictionary<NSString *, NSString *> *properties;
    return properties = properties ? properties : @{
                                                    @"bronze": @"bronze",
                                                    @"silver": @"silver",
                                                    @"gold": @"gold",
                                                    };
}

+ (instancetype)fromJSONDictionary:(NSDictionary *)dict
{
    return dict ? [[STKBadgeCounts alloc] initWithJSONDictionary:dict] : nil;
}

- (instancetype)initWithJSONDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        [self setValuesForKeysWithDictionary:dict];
    }
    return self;
}

- (NSDictionary *)JSONDictionary
{
    return [self dictionaryWithValuesForKeys:STKBadgeCounts.properties.allValues];
}
@end

NS_ASSUME_NONNULL_END

