//
//  User.h
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import <Foundation/Foundation.h>

@class STKPage;
@class STKUser;
@class STKBadgeCounts;
@class STKUserType;

NS_ASSUME_NONNULL_BEGIN

#pragma mark - Boxed enums

@interface STKUserType : NSObject
@property (nonatomic, readonly, copy) NSString *value;
+ (instancetype _Nullable)withValue:(NSString *)value;
+ (STKUserType *)moderator;
+ (STKUserType *)registered;
@end

#pragma mark - Object interfaces

@interface STKPage : NSObject
@property (nonatomic, copy)   NSArray<STKUser *> *users;
@property (nonatomic, assign) BOOL hasMore;
@property (nonatomic, assign) NSInteger quotaMax;
@property (nonatomic, assign) NSInteger quotaRemaining;

+ (_Nullable instancetype)fromJSON:(NSString *)json encoding:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
+ (_Nullable instancetype)fromData:(NSData *)data error:(NSError *_Nullable *)error;
- (NSString *_Nullable)toJSON:(NSStringEncoding)encoding error:(NSError *_Nullable *)error;
- (NSData *_Nullable)toData:(NSError *_Nullable *)error;
@end

@interface STKUser : NSObject
@property (nonatomic, strong)           STKBadgeCounts *badgeCounts;
@property (nonatomic, assign)           NSInteger accountID;
@property (nonatomic, assign)           BOOL isEmployee;
@property (nonatomic, assign)           NSInteger lastModifiedDate;
@property (nonatomic, assign)           NSInteger lastAccessDate;
@property (nonatomic, assign)           NSInteger reputationChangeYear;
@property (nonatomic, assign)           NSInteger reputationChangeQuarter;
@property (nonatomic, assign)           NSInteger reputationChangeMonth;
@property (nonatomic, assign)           NSInteger reputationChangeWeek;
@property (nonatomic, assign)           NSInteger reputationChangeDay;
@property (nonatomic, assign)           NSInteger reputation;
@property (nonatomic, assign)           NSInteger creationDate;
@property (nonatomic, assign)           STKUserType *userType;
@property (nonatomic, assign)           NSInteger userID;
@property (nonatomic, nullable, strong) NSNumber *acceptRate;
@property (nonatomic, nullable, copy)   NSString *location;
@property (nonatomic, copy)             NSString *websiteURL;
@property (nonatomic, copy)             NSString *link;
@property (nonatomic, copy)             NSString *profileImage;
@property (nonatomic, copy)             NSString *displayName;
@end

@interface STKBadgeCounts : NSObject
@property (nonatomic, assign) NSInteger bronze;
@property (nonatomic, assign) NSInteger silver;
@property (nonatomic, assign) NSInteger gold;
@end

NS_ASSUME_NONNULL_END

