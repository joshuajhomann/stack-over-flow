//
//  APIService.m
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import "APIService.h"


@implementation APIService

static NSString* getUsersPath = @"https://api.stackexchange.com/2.2/users?site=stackoverflow";

#pragma mark - Static Methods

+ (void)getUserPageWithCompletion: (void(^)(STKPage*, NSError*)) completion {
    NSURL* url = [[NSURL alloc] initWithString:getUsersPath];
    [[NSURLSession.sharedSession dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSError* jsonError;
            STKPage *page = [STKPage fromData:data error:&jsonError];
            completion(page, jsonError);
        } else {
            completion(nil, error);
        }
    }] resume];
}

@end

