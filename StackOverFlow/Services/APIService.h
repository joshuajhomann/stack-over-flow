//
//  APIService.h
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STKUser.h"


NS_ASSUME_NONNULL_BEGIN

@interface APIService : NSObject

+ (void)getUserPageWithCompletion: (void(^)(STKPage*, NSError*)) completion;

@end

NS_ASSUME_NONNULL_END

