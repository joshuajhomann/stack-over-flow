//
//  UserDetailViewController.h
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STKUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface STKAddUserViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *addUserButton;
@property (weak, nonatomic) IBOutlet UIScrollView *inputScrollView;
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *reputationTextField;
@property (weak, nonatomic) IBOutlet UITextField *goldTextField;
@property (weak, nonatomic) IBOutlet UITextField *silverTextField;
@property (weak, nonatomic) IBOutlet UITextField *bronzeTextField;

@property (nonatomic, strong) STKUser* user;

@end

NS_ASSUME_NONNULL_END
