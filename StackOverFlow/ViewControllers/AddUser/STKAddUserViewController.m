//
//  UserDetailViewController.m
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import "STKAddUserViewController.h"

@interface STKAddUserViewController () <UITextFieldDelegate, UIScrollViewDelegate>
@property NSObject* keyboardShowNotificationToken;
@property NSObject* keyboardHideNotificationToken;
@end

@implementation STKAddUserViewController

#pragma mark - UIViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.user = [STKUser new];
    self.user.badgeCounts = [STKBadgeCounts new];
    self.user.profileImage = @"";
    
    __weak typeof(self) weakSelf = self;
    self.keyboardShowNotificationToken = [[NSNotificationCenter defaultCenter]
         addObserverForName:UIKeyboardWillShowNotification
         object:nil queue:[NSOperationQueue mainQueue]
         usingBlock:^(NSNotification* notification) {
             NSDictionary* keyboardInfo = [notification userInfo];
             CGFloat height = [[keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size.height;
             weakSelf.inputScrollView.contentInset = UIEdgeInsetsMake(0, 0, height, 0);
         }];
    self.keyboardHideNotificationToken = [[NSNotificationCenter defaultCenter]
         addObserverForName:UIKeyboardWillHideNotification
         object:nil queue:[NSOperationQueue mainQueue]
         usingBlock:^(NSNotification* notification) {
             NSDictionary* keyboardInfo = [notification userInfo];
             CGFloat speed = [[keyboardInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
             [UIView animateWithDuration:speed animations:^{
                 weakSelf.inputScrollView.contentInset = UIEdgeInsetsZero;
             }];

         }];
}

#pragma mark - IBAction
- (IBAction)editingDidBegin:(UITextField *)sender {
    CGRect rect = [self.inputScrollView convertRect:sender.bounds fromView:sender];
    [self.inputScrollView scrollRectToVisible:rect animated:YES];
}

- (IBAction)tapScrollView:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* proposedText = [textField.text stringByReplacingCharactersInRange:range withString: string];
    textField.text = proposedText;
    if (textField == self.userNameTextField) {
        self.user.displayName = proposedText;
    }
    if (textField == self.reputationTextField) {
        self.user.reputation = proposedText.integerValue;
    }
    if (textField == self.goldTextField) {
        self.user.badgeCounts.gold = proposedText.integerValue;
    }
    if (textField == self.silverTextField) {
        self.user.badgeCounts.silver = proposedText.integerValue;
    }
    if (textField == self.bronzeTextField) {
        self.user.badgeCounts.bronze = proposedText.integerValue;
    }
    self.addUserButton.enabled = ([self.userNameTextField.text length] != 0) &&
                                ([self.reputationTextField.text length] != 0) &&
                                ([self.goldTextField.text length] != 0) &&
                                ([self.silverTextField.text length] != 0) &&
                                ([self.bronzeTextField.text length] != 0);

    return NO;
}
@end
