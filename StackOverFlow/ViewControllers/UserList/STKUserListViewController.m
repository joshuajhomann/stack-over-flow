//
//  UserListViewController.m
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import "STKUserListViewController.h"
#import "STKAddUserViewController.h"
#import "STKUserTableViewCell.h"
#import "APIService.h"


@interface STKUserListViewController () <UITableViewDataSource>

@end

@implementation STKUserListViewController

#pragma mark - UIViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    self.users = [NSMutableArray new];
    [APIService getUserPageWithCompletion: ^(STKPage* page, NSError* error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.users addObjectsFromArray: page.users];
            [self.tableView reloadData];
        });
    }];

}

#pragma mark - IBAction
-(IBAction)prepareForUnwind:(UIStoryboardSegue *)segue {
    if ([segue.sourceViewController isKindOfClass:[STKAddUserViewController class]]) {
        STKAddUserViewController* source = segue.sourceViewController;
        [self.users addObject: source.user];
        [self.users sortUsingComparator: ^NSComparisonResult(id lhs, id rhs) {
            return ((STKUser*)lhs).reputation < ((STKUser*)rhs).reputation;
        }];
        [self.tableView reloadData];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.users.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    STKUser *user = [self.users objectAtIndex:indexPath.row];
    STKUserTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier: NSStringFromClass(STKUserTableViewCell.class)];
    [cell configureWithUser: user];
    return cell;
}
@end
