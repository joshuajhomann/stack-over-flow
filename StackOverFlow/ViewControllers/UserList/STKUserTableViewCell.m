//
//  STKUserTableViewCell.m
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import "STKUserTableViewCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

static NSNumberFormatter* formatter;
static NSString *kFormatterLock = @"kFormatterLock";

@implementation STKUserTableViewCell

#pragma mark - Instance Methods
- (void)configureWithUser:(STKUser*) user {
    self.userImageView.image = nil;
    //Note there is no need to implement prepareForReuse to cancel the request because
    //sd_setWebImage cancels pending requests for the imageView on line 64 of UIView + WebCache.m
    [self.userImageView
     sd_setImageWithURL:[[NSURL alloc] initWithString:user.profileImage]
     placeholderImage: [UIImage imageNamed:@"avatar"]
    ];
    self.userNameLabel.text = user.displayName;
    if (formatter == nil) {
        @synchronized (kFormatterLock) {
            formatter = [NSNumberFormatter new];
            formatter.numberStyle = NSNumberFormatterDecimalStyle;
        }
    }
    self.reputationLabel.text = [formatter stringFromNumber: @(user.reputation)];
    self.goldLabel.text = @(user.badgeCounts.gold).stringValue;
    self.silverLabel.text = @(user.badgeCounts.silver).stringValue;
    self.bronzeLabel.text = @(user.badgeCounts.bronze).stringValue;
}

@end
