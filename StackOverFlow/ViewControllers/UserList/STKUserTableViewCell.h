//
//  STKUserTableViewCell.h
//  StackOverFlow
//
//  Created by Joshua Homann on 3/27/19.
//  Copyright © 2019 com.josh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STKUser.h"

NS_ASSUME_NONNULL_BEGIN

@interface STKUserTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *reputationLabel;
@property (weak, nonatomic) IBOutlet UILabel *goldLabel;
@property (weak, nonatomic) IBOutlet UILabel *silverLabel;
@property (weak, nonatomic) IBOutlet UILabel *bronzeLabel;
- (void)configureWithUser:(STKUser*) user;

@end

NS_ASSUME_NONNULL_END
